

![logo][logo]





 
# cloudformation-api 


 
A CloudFormation template that creates an API Gateway, Lambda functions, and a DynamoDB table. 


---


## Screenshots


![1-cloudformation-info-stack.png](docs/screenshots/1-cloudformation-info-stack.png) |
|:--:|
| *CloudFormation info stack* |


![2-cloudformation-events.png](docs/screenshots/2-cloudformation-events.png) |
|:--:|
| *CloudFormation events* |


![3-cloudformation-resources.png](docs/screenshots/3-cloudformation-resources.png) |
|:--:|
| *CloudFormation resources* |


![4-api-gateway-stages.png](docs/screenshots/4-api-gateway-stages.png) |
|:--:|
| *API Gateway stages* |


![5-lambda-functions.png](docs/screenshots/5-lambda-functions.png) |
|:--:|
| *Lambda functions* |


![6-dynamodb-tables.png](docs/screenshots/6-dynamodb-tables.png) |
|:--:|
| *DynamoDB tables* |


![7-dynamodb-table-items.png](docs/screenshots/7-dynamodb-table-items.png) |
|:--:|
| *DynamoDB table items* |









## Requirements

In order to run this project you will need: 

- [AWS][aws] - Amazon Web Services (AWS) is a secure cloud services platform, offering compute power, database storage, content delivery and other functionality to help businesses scale and grow. Explore how millions of customers are currently leveraging AWS cloud products and solutions to build sophisticated applications with increased flexibility, scalability and reliability.




## Usage

Upload the CloudFormation template `cf.json` to your CloudFormation, and create a stack.
After the stack is created you can query the API.

To list all the customers :
```
https://vk313coghj.execute-api.ap-southeast-2.amazonaws.com/dev/customers/
```

To query a specific customer:
```
https://vk313coghj.execute-api.ap-southeast-2.amazonaws.com/dev/customers/id?Customer=Amazon&Address=Perth,Australia

```











## References

For additional context, refer to some of these links. 

- [AWS Lambda](https://aws.amazon.com/lambda/) - AWS Lambda lets you run code without provisioning or managing servers. You pay only for the compute time you consume - there is no charge when your code is not running.
- [AWS DynamoDB](https://aws.amazon.com/dynamodb/) - Amazon DynamoDB is a key-value and document database that delivers single-digit millisecond performance at any scale. It's a fully managed, multiregion, multimaster database with built-in security, backup and restore, and in-memory caching for internet-scale applications.
- [AWS API Gateway](https://aws.amazon.com/api-gateway/) - Amazon API Gateway is a fully managed service that makes it easy for developers to create, publish, maintain, monitor, and secure APIs at any scale. With a few clicks in the AWS Management Console, you can create REST and WebSocket APIs that act as a “front door” for applications to access data, business logic, or functionality from your backend services, such as workloads running on Amazon Elastic Compute Cloud (Amazon EC2), code running on AWS Lambda, any web application, or real-time communication applications.
- [AWS CloudFormation](https://aws.amazon.com/cloudformation/) - AWS CloudFormation provides a common language for you to describe and provision all the infrastructure resources in your cloud environment.




## Resources

Resources used to create this project: 

- [Photo](https://unsplash.com/photos/J4Ui2ch3oRU) - Photo by Parth Vyas on Unsplash
- [Gitignore.io](https://gitignore.io) - Defining the `.gitignore`
- [LunaPic](https://www341.lunapic.com/editor/) - Image editor (used to create the avatar)





## Repository

We use [SemVer](http://semver.org/) for versioning. 

- **[Branches][branches]**
- **[Commits][commits]**
- **[Tags][tags]**
- **[Contributors][contributors]**
- **[Graph][graph]**
- **[Charts][charts]**









## Contributors

Thank you so much for making this project possible: 

- [Valter Silva](https://gitlab.com/valter-silva)



## Copyright

Copyright © 2019-2019 [SkaleSys][company]





[logo]: docs/logo.jpeg


[company]: https://skalesys.com
[contact]: https://skalesys.com/contact
[services]: https://skalesys.com/services
[industries]: https://skalesys.com/industries
[training]: https://skalesys.com/training
[insights]: https://skalesys.com/insights
[about]: https://skalesys.com/about
[join]: https://skalesys.com/Join-Our-Team

[ansible]: https://ansible.com
[terraform]: http://terraform.io
[packer]: https://www.packer.io
[docker]: https://www.docker.com/
[vagrant]: https://www.vagrantup.com/
[kubernetes]: https://kubernetes.io/
[spinnaker]: https://www.spinnaker.io/
[jenkins]: https://jenkins.io/
[aws]: https://aws.amazon.com/
[ubuntu]: https://ubuntu.com/




[aws]: https://aws.amazon.com/






[branches]: https://gitlab.com/skalesys/cloudformation-api/branches
[commits]: https://gitlab.com/skalesys/cloudformation-api/commits
[tags]: https://gitlab.com/skalesys/cloudformation-api/tags
[contributors]: https://gitlab.com/skalesys/cloudformation-api/graphs
[graph]: https://gitlab.com/skalesys/cloudformation-api/network
[charts]: https://gitlab.com/skalesys/cloudformation-api/charts


