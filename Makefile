SHELL = /bin/sh
export ROOTS_VERSION=master

export COMPANY_NAME=SkaleSys
export COMPANY_WEBSITE=https://skalesys.com

export LOGO_THEME=technology,api

export PROJECT_NAME=cloudformation-api

-include $(shell [ ! -d .roots ] && git clone --branch $(ROOTS_VERSION) https://gitlab.com/skalesys/roots.git .roots; echo .roots/Makefile)
