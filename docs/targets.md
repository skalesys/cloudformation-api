## Makefile targets

```Available targets:

  aws-nuke/install                   	Install aws-nuke
  base                               	Runs base playbook
  clean                              	Clean roots
  docker/install                     	Install docker
  gomplate/install                   	Install gomplate
  google-chrome/install              	Install google-chrome
  help/all                           	Display help for all targets
  help/all/plain                     	Display help for all targets
  help                               	Help screen
  help/short                         	This help short screen
  java/install                       	Install java
  openvpn/install                    	Install openvpn
  packer/install                     	Install packer
  packer/version                     	Prints the packer version
  pip/install                        	Install pip
  readme                             	Alias for readme/build
  readme/build                       	Create README.md by building it from README.yaml
  readme/install                     	Install README
  security/install                   	Install security
  spotify/install                    	Install spotify
  terraform/apply                    	Builds or changes infrastructure
  terraform/clean                    	Cleans Terraform vendor from Maker
  terraform/console                  	Interactive console for Terraform interpolations
  terraform/destroy                  	Destroy Terraform-managed infrastructure, removes .terraform and local state files
  terraform/fmt                      	Rewrites config files to canonical format
  terraform/get                      	Download and install modules for the configuration
  terraform/graph                    	Create a visual graph of Terraform resources
  terraform/init-backend             	Initialize a Terraform working directory with S3 as backend and DynamoDB for locking
  terraform/init                     	Initialize a Terraform working directory
  terraform/install                  	Install terraform
  terraform/output                   	Read an output from a state file
  terraform/plan                     	Generate and show an execution plan
  terraform/providers                	Prints a tree of the providers used in the configuration
  terraform/push                     	Upload this Terraform module to Atlas to run
  terraform/refresh                  	Update local state file against real resources
  terraform/show                     	Inspect Terraform state or plan
  terraform/taint                    	Manually mark a resource for recreation
  terraform/untaint                  	Manually unmark a resource as tainted
  terraform/validate                 	Validates the Terraform files
  terraform/version                  	Prints the Terraform version
  terraform/workspace                	Select workspace
  update                             	Updates roots
  vagrant/destroy                    	Stops and deletes all traces of the vagrant machine
  vagrant/install                    	Install vagrant
  vagrant/recreate                   	Destroy and creates the vagrant environment
  vagrant/update/boxes               	Updates all Vagrant boxes
  vagrant/up                         	Starts and provisions the vagrant environment
  version                            	Displays versions of many vendors installed
  virtualbox/install                 	Install virtualbox
  vlc/install                        	Install vlc
```
